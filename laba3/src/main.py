import argparse
from polinom import Vvod
from polinom import Calculation

parser = argparse.ArgumentParser()
parser.add_argument('--poly', type=str)
args = parser.parse_args()

V = Vvod()
C = Calculation()

y = C.raschet(V.parser(args.poly))
print(y)
